"""
Configure visualization elements and instantiate a server
"""
from .model import BotnetModel, BotAgent, BotState, BotmasterState

from mesa.visualization.ModularVisualization import ModularServer
from mesa.visualization.UserParam import UserSettableParameter
from mesa.visualization.modules import ChartModule, NetworkModule, TextElement

def state_color(state):
    # Google Color Palette
    return {
        BotState.SUCEPTIBLE: "#34ebde",
        BotState.PASSIVE: "#008744",
        BotState.EMISOR: "#ffa700",
        BotState.RECEPTOR: "#0057e7",
        BotState.OFFLINE: "#000000",
        BotmasterState.INFECT: "#ed894d",
        BotmasterState.ACT: "#d62d20"
    }.get(state)

def network_portrayal(G):
    portrayal = dict()

    portrayal['nodes'] = [
        {
            "size": 6,
            "color": state_color(agents[0].state),
            "tooltip": "id: {}<br>state: {}".format(
                agents[0].unique_id, agents[0].state.name
            ),
        }
        for (_, agents) in G.nodes.data("agent")
    ]

    portrayal['edges'] = [
        {
            "source": source,
            "target": target,
            "color": "#000000",
            "width": 2,
        }
        for (source, target) in G.edges
    ]

    return portrayal

network = NetworkModule(network_portrayal, 500, 500, library='d3')

chart = ChartModule([
    {"Label": "Suceptible", "Color": state_color(BotState.SUCEPTIBLE)},
    {"Label": "Passive", "Color": state_color(BotState.PASSIVE)},
    {"Label": "Receptor", "Color": state_color(BotState.RECEPTOR)},
    {"Label": "Emisor", "Color": state_color(BotState.EMISOR)},
    {"Label": "Offline", "Color": state_color(BotState.OFFLINE)},
    {"Label": "TotalInfected", "Color": '#f5429e'}
])

def buildProbabilitySetter(name, description, value=0.5):
    return UserSettableParameter(
        param_type="slider",
        name=name,
        value=value, min_value=0, max_value=1, step=0.01,
        description=description,
    )

params = {
    "num_nodes": UserSettableParameter(
        param_type="slider",
        name="Number of nodes",
        value=10, min_value=10, max_value=500,
        step=1,
        description="How many nodes are in the network",
    ),
    "infection_coeff": buildProbabilitySetter(
        name="Infection coefficient",
        description="Probability of getting infected after interating with bot"
    ),
    "loss_coeff": buildProbabilitySetter(
        name="Loss coefficient",
        description="Probability of shutting down an agent",
        value=0.01
    ),
    "emisor_coeff": buildProbabilitySetter(
        name="Emisor coefficient",
        description="Probability of becoming an emisor bot"
    ),
    "receptor_coeff": buildProbabilitySetter(
        name="Receptor coefficient",
        description="Probability of becoming an emisor bot"
    ),
    "eager_coeff": buildProbabilitySetter(
        name="Eager coefficient",
        description="Botmaster probability of initating the attack",
        value=0.01
    ),
    "modifier_prob": buildProbabilitySetter(
        name="Adyacency modifier probability",
        description="Ratio of how random the network will be"
    )
}

server = ModularServer(
    model_cls=BotnetModel,
    visualization_elements=[network, chart],
    name="Botnet spreading on P2P network",
    model_params=params
)