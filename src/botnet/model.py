from enum import Enum

from functools import partial

import math

from mesa import Agent, Model
from mesa.time import RandomActivation
from mesa.datacollection import DataCollector
from mesa.space import NetworkGrid

import networkx as nx

class BotState(Enum):
    """
    Bot States.
    """

    SUCEPTIBLE = 0,
    PASSIVE = 1,
    RECEPTOR = 2,
    EMISOR = 3,
    OFFLINE = 4,


class BotAgent(Agent):  # noqa
    """
    A bot agent.

    Attributes:
        unique_id(int): identifier of the node
        model(BotnetModel): network where the bot exists
        state(BotState): current state
        infection_coeff(float): probability of infection
        loss_coeff(float): probability of shutting down the current agent.
        emisor_coef(float): probability of becoming an emissor bot
        receptor_coeff(float): probability of becoming a receptor bot 
    """
    def __init__(
        self, 
        unique_id,
        model,
        initial_state,
        infection_coeff,
        loss_coeff,
        emisor_coeff,
        receptor_coeff
        ):
        super().__init__(unique_id, model)
        self.state = initial_state
        self.infection_coeff = infection_coeff
        self.loss_coeff = loss_coeff
        # TODO: change const by historical metric concerning emissor occurence
        self.emisor_coeff = emisor_coeff
        # TODO: change const by historical metric concerning receptor occurence
        self.receptor_coeff = receptor_coeff

    def frac_neighbors(self, test):
        """
        Fraction of the neighbors passing certain condition

        Arguments:
            test(function): BotAgent filter
        """
        nodes = self.model.grid.get_cell_list_contents(
            self.model.grid.get_neighbors(self.pos)
        )
        return len([n for n in nodes if test(n)]), len (nodes)

    def _transition_with_neighbors(self, test_nei, calc_prob, nx_st):
        """
        Given a neighbors filter and a probability based on the filtered
        fraction of neighbors, the current states changes to the given one with
        the calculated probability

        Arguments:
            test_nei(function): BotAgent filter
            calc_prob(function): probability calculation function
            nx_st(BotState): tentative next state
        """
        pss, total = self.frac_neighbors(test_nei)
        if self.random.uniform(0, 1) < calc_prob(pss, total):
            self.state = nx_st

    def _try_change_role(self, count_st, nx_st, coeff):
        """
        Given a current state, tries to change to a next state with a
        probability related to the given coefficient

        Arguments:
            prev_st(BotState): current state
            nx_st(BotState): tentative next state
            coeff(float): coefficient to calculat the transition probability
        """
        self._transition_with_neighbors(
            test_nei= lambda n: n.state == count_st,
            calc_prob= (lambda pss, total:
                coeff if self.master_in_neighbors(state=BotmasterState.ACT)
                else (pss/total) * coeff
            ),
            nx_st=nx_st
        )

    def try_self_infect(self):
        """
        If not infected and available, will become infected with a probability
        proportional to the amount of infected neigbors.
        """
        self._transition_with_neighbors(
            test_nei=lambda n:
                n.state not in [BotState.SUCEPTIBLE, BotState.OFFLINE],
            calc_prob=lambda pss, total: 1 - (1-self.infection_coeff)**(pss),
            nx_st=BotState.PASSIVE
        )

    def try_disconnect(self):
        """
        Disconnects with a certain probability.
        """
        # TODO: change to some attack interaction metric
        if self.random.uniform(0, 1) < self.loss_coeff:
            self.state = BotState.OFFLINE

    def try_activate_role(self):
        """
        Changes from passive to either receptor or emissor
        """
        # TODO: change to some statistical metric concerning historical data
        c = 0.5
        if self.random.uniform(0, 1) < c:
            self.try_emissor()
        else:
            self.try_receptor()

    def try_emissor(self):
        """
        Becomes a bot emissor with certain probability
        """
        self._try_change_role(
            count_st=BotState.RECEPTOR,
            nx_st=BotState.EMISOR,
            coeff=self.receptor_coeff
        )

    def try_receptor(self):
        """
        Becomes receptor with certain probability
        """
        self._try_change_role(
            count_st=BotState.EMISOR,
            nx_st=BotState.RECEPTOR,
            coeff=self.emisor_coeff
        )

    def try_connect(self):
        """
        Connects with a certain probability.
        """
        # TODO: change to some attack interaction metric
        if self.random.uniform(0, 1) < 1 - self.loss_coeff:
            self.state = BotState.SUCEPTIBLE

    def master_in_neighbors(self, state):
        """
        Checks if the master 
        """
        (pss, _) = self.frac_neighbors(lambda n: n.state==state)
        return pss != 0

    def step(self):
        """
        Tries to transition to other state with a certain probability given the 
        current state.
        """
        action = {
            BotState.SUCEPTIBLE: lambda : self.try_self_infect(),
            BotState.PASSIVE: lambda: self.try_activate_role(),
            BotState.RECEPTOR: lambda: self.try_emissor(),
            BotState.EMISOR: lambda: self.try_receptor(),
            BotState.OFFLINE: lambda: self.try_connect()
        }.get(self.state)
        action()
        self.try_disconnect()

class BotmasterState(Enum):
    """
    Botmaster states
    """
    INFECT = 0,
    ACT = 1,

class BotmasterAgent(Agent):
    """
    The botmaster agent.

    Attributes:
        unique_id(int): identifier of the node
        model(BotnetModel): network where the bot exists
        state(BotState): current state
        eager_coeff(float): probability of starting the attack
    """
    def __init__(self, unique_id, model, initial_state, eager_coeff):
        super().__init__(unique_id, model)
        self.state = initial_state
        self.eager_coeff = eager_coeff

    def step(self):
        """
        Changes from growing botnet to attack with a certain probability.
        """
        nodes = self.model.grid.get_cell_list_contents(
            self.model.grid.get_neighbors(self.pos)
        )
        inf = len([n for n in nodes if n.state==BotState.PASSIVE]) / len (nodes)
        
        if self.random.uniform(0, 1) < inf * self.eager_coeff:
            self.state = BotmasterState.ACT

def number_state(model, state):
    return sum([
        1 for a in model.grid.get_all_cell_contents() if a.state is state
    ])

def number_infected(model):
    return sum([
        1 for a in model.grid.get_all_cell_contents() if a.state not in 
        [BotState.SUCEPTIBLE, BotState.OFFLINE]
    ])

class BotnetModel(Model):
    """
    This represents a exisiting P2P network at the begining of an hybrid botner
    attack.

    There is one botmaster at who will try to take over the botnet to launch an
    attack.
    """

    def __init__(
        self,
        num_nodes,
        infection_coeff,
        loss_coeff,
        emisor_coeff,
        receptor_coeff,
        eager_coeff,
        modifier_prob,
        ):

        super().__init__()
        self.num_nodes = num_nodes
        self.schedule = RandomActivation(self)

        # TODO: check the nature of the dataset network
        self.G = nx.barabasi_albert_graph(
            self.num_nodes,
            min(1, math.floor(self.num_nodes*modifier_prob))
        )
        self.grid = NetworkGrid(self.G)

        master_node_id = self.random.randint(0, self.num_nodes)

        for i, node in enumerate(self.G):
            agent = None
            if i == master_node_id:
                agent = BotmasterAgent(
                unique_id=self.num_nodes,
                model=self,
                initial_state=BotmasterState.INFECT,
                eager_coeff=eager_coeff
                )
                
            else:
                agent = BotAgent(
                    unique_id=i,
                    model=self,
                    initial_state=BotState.SUCEPTIBLE,
                    infection_coeff=infection_coeff,
                    loss_coeff=loss_coeff,
                    emisor_coeff=emisor_coeff,
                    receptor_coeff=receptor_coeff
                    )
            self.schedule.add(agent)

            self.grid.place_agent(agent, node)

        # example data collector
        self.datacollector = DataCollector(
            model_reporters={
                "Suceptible": partial(number_state, state=BotState.SUCEPTIBLE),
                "Passive": partial(number_state, state=BotState.PASSIVE),
                "Receptor": partial(number_state, state=BotState.RECEPTOR),
                "Emisor": partial(number_state, state=BotState.EMISOR),
                "TotalInfected": number_infected,
                "Offline": partial(number_state, state=BotState.OFFLINE)
            }
        )

        self.running = True
        self.datacollector.collect(self)

    def step(self):
        """
        A model step. Used for collecting data and advancing the schedule
        """
        self.datacollector.collect(self)
        self.schedule.step()
